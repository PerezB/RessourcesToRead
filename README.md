# Ressources to read

## About one video game

- [x] The Heart of Dead Cells: A visual making-of, Benoit "exserv" Reinier, Third Edition. 

## About video games industry

- [ ] https://ca.ign.com/articles/2012/07/16/is-metacritic-ruining-the-games-industry
- [ ] http://www.roguebasin.com/index.php?title=Berlin_Interpretation

## About technical aspects

### Code in general

- [ ] Clean Code: A Handbook of Agile Software Craftsmanship, Robert C.Martin

### Graphics

- [ ] https://www.gamasutra.com/view/news/313026/Art_Design_Deep_Dive_Using_a_3D_pipeline_for_2D_animation_in_Dead_Cells.php
- [ ] https://www.gamasutra.com/view/news/315345/Art_Design_Deep_Dive_Giving_back_colors_to_cryptic_worlds_in_Dead_Cells.php

### Network

- [ ] Multiplayer Game Programming: Architecting Networked Games, Glazer Madhav and Joshua Glazer Sanjay Madhav, Addison-Wesley Professional
- [ ] https://gafferongames.com/post/introduction_to_networked_physics/

### Maths

- [ ] Real Time Collision Detection, Christer Ericson, 2004

### Game Engine

- [ ] Game Engine Architecture, Jason Gregory, 2014

### IA 

- [ ] Programming Game AI by Example, Mat Buckland, 2004